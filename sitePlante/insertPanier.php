<?php 
    include("connexion.php");//fichier de connexion a la BDD
    include("navbar.php");//besoin de ce fichier car il contient l'idUser

    $qty = intval($_POST['qty']);//récupere la quantité de produit a ajouter au panier
    $idPlante = intval($_POST['id_plante']);// récupere l'id plante qui va etre ajouter au panier

        if($idUser != NULL)// si l'utilisateur est connecté :
        {   

            $query5 = $mysqli -> query("SELECT id_plante FROM PANIER where id_plante = $idPlante AND id_utilisateur = $idUser"); //on va vérifier si le produit existe dans le panier
            $checkExistBDD = $query5 -> fetch_array(MYSQLI_ASSOC);
            $checkBDD = $checkExistBDD['id_plante'];

            if ($checkBDD == ''){// si il n'éxiste pas on l'ajoute 
                $req = "INSERT INTO PANIER (id_panier, id_utilisateur, id_plante, qty_panier) VALUES (0,$idUser,$idPlante, $qty)";
                $insert = $mysqli -> query($req);
                header("Location: boutique.php");
                header("Refresh:0");
                echo "<script> window.location.reload(); </script>";
             }
            else{ //si non on ajoute la quantite au produit
                $reqUpdate = "UPDATE PANIER SET qty_panier = qty_panier + $qty WHERE id_plante = $idPlante";
                $updatePanier = $mysqli -> query($reqUpdate);
                header("Refresh:0");
                echo "<script> window.location.reload(); </script>";
            }
        }
        else
        {
            echo "<script> Vous devez être connecté à votre compte pour ajouter un produit au panier";
        }
          // Actualise votre page actuelle
    header("Refresh:0");
    echo "<script> window.location.reload(); </script>";
?>