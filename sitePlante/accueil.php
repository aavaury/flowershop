<?php
    session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" type="text/css" href="css/style.css" />
        <link href="https://fonts.googleapis.com/css?family=Slabo+27px" rel="stylesheet">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    </head>
    <?php include('navbar.php');?>
    <body>

</body>
<html>

       <div class="wrapper">
           <div class="container" style="
            margin: 0!important;
            padding: 0;
            max-width: -webkit-fill-available;">

               <div class="slideshow">

                    <div class="slideshow-left">

                        <div class="Lslide">
                            <div class="Lslide-content">
                                <h2>FlowerShop - Acceuil</h2>
                                <p>Découvrez nos produits haute gamme</p>

                                <div class="button">
                                    <a href="boutique.php">
                                        <p>Découvrir</p>
                                        <i class="fa fa-chevron-right" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="Lslide">
                            <div class="Lslide-content">
                                <h1>Des plantes du monde entier</h1>
                                <p>Il y en a pout tous les goûts</p>
                                <h2>Décoration, plantes d'intérieur, éxtérieur...</h2>
                                <div class="button">
                                    <a href="boutique.php">
                                        <p>Découvrir</p>
                                        <i class="fa fa-chevron-right" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="Lslide">
                            <div class="Lslide-content">
                                <h2>Des plantes fabuleuses à offrir ou a vous offrir !</h2>

                                <div class="button">
                                    <a href="boutique.php">
                                        <p>Découvrir</p>
                                        <i class="fa fa-chevron-right" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                        </div>    

                    </div>

                    <div class="slideshow-right">

                        <div class="Rslide">
                            <img src="img/flower-3.jpg">
                        </div>

                        <div class="Rslide">
                            <img src="img/flower-5.jpg">
                        </div>     
                        <div class="Rslide">
                            <img src="img/flower-1.jpg">
                        </div>                                              
                    </div>    

                    
                    <div class="control">
                        <div class="oncontrol control-top">
                            <i class="fa fa-arrow-up" aria-hidden="true"></i>
                        </div>
                        <div class="oncontrol control-bottom">
                            <i class="fa fa-arrow-down" aria-hidden="true"></i>
                        </div>                          
                    </div>

               </div>

           </div>
       </div>


        <script type="text/javascript" src="js/script.js"></script>
    </body>
</html>
<?php
  include("navbar.php");
?>


<style>
  body,html {
    width: 100%;
    height: 100%;
    padding: 0;
    margin: 0;
    display: block;
    position: relative;
    overflow: hidden;
    font-family: 'Slabo 27px', serif;
    font-size: 16px;
}

h1,h2,h3,h4,h5,h6 {
    padding: 0;
    margin: 0;
}
ul,li,p {
    margin: 0;
    padding: 0;
}
a {
    text-decoration: none;
}
.wrapper {
    height: 100%;
    width: 100%;
    display: block;
    overflow: hidden;
}
.wrapper .container {
    height: 100%;
    width: 100%;
    display: block;
    overflow: hidden;
}

.slideshow {
    height: 100%;
    width: 100%;
    display: block;
    overflow: hidden;
}
.slideshow .slideshow-left {
    width: 40%;
    float: left;
    position: relative;
    height: 100%;
}
.slideshow .slideshow-left .Lslide {
    height: 100%;
    display: block;
    overflow: hidden;
    position: absolute;
    bottom: 0px;
    top: 0;
    left: 0;
    right: 0;
}  
.slideshow .Lslide-content {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    -ms-transform: translate(-50%, -50%);
    -moz-transform: translate(-50%, -50%);
    -webkit-transform: translate(-50%, -50%);
}
.slideshow .Lslide-content h2 {
    color: #fff;
    font-weight: 600;
    font-size: 60px;
    line-height: 0.8;
}
.slideshow .Lslide-content p {
    color: #fff;
    padding: 2.5em 0em;
}
.slideshow .Lslide-content .button {
    background-color: #fff;
    display: inline-block;
    border-radius: 25px;
    position: relative;
}
.slideshow .Lslide-content .button i {
    float: right;
    position: absolute;
    top: 50%;
    right: 2.5em;
    color: #000;
    font-size: 9px;
    transform: translateY(-50%);
    -ms-transform: translateY(-50%);
    -moz-transform: translateY(-50%);
    -webkit-transform: translateY(-50%);
    transition: all 0.3s;
}
.slideshow .Lslide-content .button:hover i {
    right: 1.7em;
}
.slideshow .Lslide-content .button p {
    margin: 0;
    padding: 0;
    color: #000;
    text-align: left;
    padding: 0.8em 8em 0.8em 1.5em;
    font-size: 12px;
    font-weight: 600;
}
.slideshow .slideshow-right {
    width: 60%;
    float: left;
    position: relative;
    height: 100%;    
}     
.slideshow .slideshow-right .Rslide {
    height: 100%;
    display: block;
    overflow: hidden;
    position: absolute;
    bottom: 0px;
    top: 0;
    left: 0;
    right: 0;
}  
.slideshow .slideshow-right .Rslide img {
    min-width: 100%;
}
.control {
    position: absolute;
    left: 40%;
    top: 50%;
    transform: translateY(-50%);
    -ms-transform: translateY(-50%);
    -moz-transform: translateY(-50%);
    -webkit-transform: translateY(-50%);
}    
.control i {
    left: 0;
    right: 0;
    top: 50%;
    transform: translateY(-50%);
    -ms-transform: translateY(-50%);
    -moz-transform: translateY(-50%);
    -webkit-transform: translateY(-50%);
    text-align: center;
    position: absolute;
    color: #aaaaaa;
    transition: all 0.3s;
    -ms-transition: all 0.3s;
    -moz-transition: all 0.3s;
    -webkit-transition: all 0.3s;
}    
.control .control-top {
    width: 45px;
    height: 50px;
    background-color: #fff;
    z-index: 5;
    position: relative;
    border-radius: 0px 4px 4px 0px;
    cursor: pointer;
}
.control .control-top:hover i {
    color: #333333;
}
.control .control-bottom {
    width: 45px;
    height: 50px;
    background-color: #fff;
    z-index: 5;
    position: relative;
    border-radius: 4px 0px 0px 4px;
    left: -45px;
    cursor: pointer;
}  
.control .control-bottom:hover i {
    color: #333333;
}              
</style>

<script>
var Lslide      = document.querySelectorAll('.Lslide'),
    Rslide      = document.querySelectorAll('.Rslide'),
    control     = document.querySelectorAll('.oncontrol'),
    slideHeight = document.querySelector('.wrapper').clientHeight,
    color = ['#fdc97c', '#e5d3d0', '#71b3d6'],
    index = 0;


function init() {
    slideHeight = document.querySelector('.wrapper').clientHeight;
    for (i = 0; i < Lslide.length; i++) {
        Lslide[i].style.backgroundColor = color[i];
        Lslide[i].style.top = -slideHeight * i + "px";
        Rslide[i].style.top = slideHeight * i + "px";
    }  
}
init()
window.addEventListener('resize', function(){
    init()
});

function moveToTop() {

    index++;
    for (el = 0; el < Lslide.length; el++) {
        Lslide[el].style.top = parseInt(Lslide[el].style.top) + slideHeight + "px";
        Rslide[el].style.top = parseInt(Rslide[el].style.top) - slideHeight + "px";
    }

    if (index > Lslide.length-1) {
        index = 0;
        for (el = 0; el < Lslide.length; el++) {
            Lslide[el].style.top = -slideHeight * el + "px";
            Rslide[el].style.top = slideHeight * el + "px";
        }
    }
}

function moveToBottom() {
    index--;
    for (el = 0; el < Lslide.length; el++) {
        Lslide[el].style.top = parseInt(Lslide[el].style.top) - slideHeight + "px";
        Rslide[el].style.top = parseInt(Rslide[el].style.top) + slideHeight + "px";
        
    }
    if (index < 0) {
        index = Rslide.length-1;
        for (el = 0; el < Lslide.length; el++) {
            Lslide[el].style.top = parseInt(Lslide[el].style.top) + slideHeight * Lslide.length + "px";
            Rslide[el].style.top = parseInt(Rslide[el].style.top) - slideHeight * Rslide.length + "px";
        }
    }
}

function miseEnAttente()
{
 //Traitement
 setTimeout(moveToBottom(), 3000); //On attend 5 secondes avant d'exécuter la fonction
}


function transition() {
    for (t = 0; t < Lslide.length; t++) {
        Lslide[t].style.transition = "all 0.8s";
        Rslide[t].style.transition = "all 0.8s";
    }
}
  

for (t = 0; t < control.length; t++) {
    control[t].addEventListener("click", function() {

        if (this.classList.contains('control-top')) {
            moveToTop()
        } 
        if (this.classList.contains('control-bottom')) {
            moveToBottom()
        }

        transition()
   
    });
}

var wheeling;



document.addEventListener("mousewheel", mousemouve);
document.addEventListener("DOMMouseScroll", mousemouve);
</script>
>>>>>>> new commit
