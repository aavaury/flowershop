<?php
include('connexion.php');
 
$id_plante = $_POST['id_plante'];//récupere l'idplante a ajouter au panier
$req_idPlante =  mysqli_query($mysqli,"SELECT * FROM BOUTIQUE where id_plante=".$id_plante);//requete pour afficher toutes les infos de la plante dans le modal
?>

<!--Modal body -->
<form action="" method="POST" class="modal-dialog">
<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title">Ajouter au panier</h4>
      <button type="button" class="close" data-dismiss="modal">×</button>
    </div>
    <div class="modal-body">
<?php
while($boutique = $req_idPlante->fetch_assoc()){
?>
<table border='0' width='100%'>
<tr>
    <td width="300"><img width="260px" heigth="160px" src="img/<?php echo $boutique['img_plante']; ?>.png">
    <td style="padding:20px;">
    <p>Nom : <?php echo $boutique['nom_plante']; ?></p>
    <p>Prix : <?php echo $boutique['prix_plante']; ?> €</p>
    <label>Quantité : </label><br>
    <input type="number" value="1" id="qty">
    </td>
</tr>
</table>
</div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Anuler</button>
      <input id="send_to_panier" class="btn btn-success" type="button" data-id="<?php echo $boutique['id_plante']; ?>" value="Ajouter" data-dismiss="modal"/>
    </div>
</div>
<?php } ?>
</form><!-- Fin modal Body--->

<script type='text/javascript'>
                $(document).on('click', '#send_to_panier', function() {
                    var id_plante = $(this).data('id');//recupere l'idplante de la plante concerné
                    var qty = document.getElementById('qty').value;//récuper la quantité saisie
                    //console.log(id_plante);
                    //console.log(qty);
                    $.ajax({
                        url: 'insertPanier.php',//envoie les valeurs dans le fichier insertPanier.php
                        type: 'post',
                        data: {id_plante: id_plante, qty: qty},
                        success: function(response){ //si tout fonctionne ajoute le modal body avec les infos dans la div modal(empModal) et ouvre le modal
                            $('#empModal').html(response); 
                            $('#empModal').modal('show'); 
                        }
                    });
                });
</script>
