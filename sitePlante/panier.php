<?php
    session_start();
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="assets/css/style.css">   
    <script type="text/javascript" src="./js/jquery/2.1.3/jquery.min.js"></script>
    <script src="jquery-3.5.1.min.js"></script> 
</head>

<?php
include('navbar.php');
?>
<body>
<br>
    <div class="blockPanier">
        
        <?php
            include('connexion.php');
            
            // afficher un message    

            if($_SESSION['username'] == ""){
                echo "vous devez vous connecter pour acceder a votre panier";
                ?>
                <a class="button" href="index.php">Se connecter</a>
                <?php
            }else {
            echo '<table class="table" style="table-layout: fixed;">';
            echo '<tr id="titleBarPanier"><th scope="col">Nom</th><th scope="col">Prix</th><th scope="col">Image</th><th scope="col">Quantité</th></tr>';


                $req_panier = mysqli_query($mysqli, "SELECT DISTINCT * FROM BOUTIQUE, PANIER where BOUTIQUE.id_plante = PANIER.id_plante AND id_utilisateur = $idUser");
   
                while ($panier = $req_panier->fetch_assoc())
                {
                    echo '<tr scope="row">';
                    echo '<td scope="col">'.$panier['nom_plante'].'</td>';
                    echo '<td id="nom_plante" scope="col">'.$panier['prix_plante'].'€</td>';  
                    echo '<td scope="col"><img class="imgPlantePanier" src="img/'.$panier['img_plante'].'.png"></img></td>';  
                    echo '<td scope="col">'.$panier['qty_panier'].'</td>';  
                    echo '<td scope="col" sytle="display:flex;"><button type="submit" data-id='.$panier['id_plante'].' id="delPanier" class="btn btn-danger">Supprimer</button></td>';
                }
     
                echo '</tr></table></form>';

            }//tableau affichant les produits au panier
    
       ?>

<script type='text/javascript'>
                $(document).on('click', '#delPanier', function(){
                    var id_plante = $(this).data('id');//recupere l'idplante de la plante concerné
                    console.log(id_plante);
                    $.ajax({
                        url: 'deletePanier.php',//envoie les valeurs dans le fichier insertPanier.php
                        type: 'post',
                        data: {id_plante: id_plante},
                        success: function(){ //si tout fonctionne ajoute le modal body avec les infos dans la div modal(empModal) et ouvre le modal
                           window.location.reload();
                        }
                    });
                });
</script>


        <div id="panier">
            <?php
            $reqPrixTotal = $mysqli -> query("SELECT SUM(prix_plante * qty_panier) FROM `PANIER` WHERE id_utilisateur = $idUser");
            $prix_totalTemp = $reqPrixTotal -> fetch_array(MYSQLI_ASSOC);
            $prix_total = intval($prix_totalTemp['SUM(prix_plante * qty_panier)']);

            echo "<h2>PRIX TOTAL : ".$prix_total." € HT</h2>";
            ?>
        </div>
    </div>
</body>
<br>
</html>


<style> 
  td{ border-style:inset; }
</style>

