#!/bin/bash

# echo coucou

# https://stackoverflow.com/questions/7265272/how-to-list-files-in-directory-using-bash
# https://stackoverflow.com/questions/407184/how-to-check-the-extension-of-a-filename-in-a-bash-script
# https://stackoverflow.com/questions/14704559/how-to-insert-image-in-mysql-databasetable

LOCAL_DIR=.

for file in $LOCAL_DIR/*
do
    if [[ -f $file ]]; then
        #echo $file

        # Exclure le fichier du script
        if [[ $file == *.png  ]] || [[ $file == *.jpg  ]]; then

            echo "INSERT INTO BOUTIQUE(id_plante, img_plante, nom_plante, prix_plante) VALUES(1,LOAD_FILE('$file'));"

        fi        
    fi
done


