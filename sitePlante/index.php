<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css">
    <title id="titre">Connexion - FlowerShop</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
</head>

    <body>

        <?php
        include('navbar.php');
        ?>

        <h1>Déjà client ? Connectez-vous</h1>
        <br><br>
        
        <?php
            if(isset($_SESSION['username'])){
                header("index.php");
            }
            else{
                header('principale.php');
            }
        ?>
        <div id="container" style="width: 25%; margin-left: auto; margin-right: auto;">
            <!-- zone de connexion -->
            <form class="formulaire_conn" action="verification.php" method="post">
            <p>Formulaire de connexion</p>
            <label><b>Nom d'utilisateur</b></label>
            <input type="text" placeholder="Entrer le nom d'utilisateur" name="username" required>

                <label><b>Mot de passe</b></label>
                <input type="password" placeholder="Entrer le mot de passe" name="password" required>

                <input type="submit" id='submit' value='Se connecter'>
                <p>Pas encore inscrit ? <a class="linkMenu" href="inscription.php" style=" color:black;"> S'inscrire</a></p>
            </form>
        </div>
        <br>
        <br>
        <br>
    </body>
</html>
