<?php
    session_start();
?>
<!DOCTYPE html>
<html lang="fr">

    <head>
        <title id="titre">FlowerShop - Boutique de plantes</title>
    </head>
    <?php
            include("navbar.php");
        ?>
    <body>


            <div id="blockSearch"><i class="icon ion-ios-search-strong" style="font-size:7vh;"></i><input type="text" id="searchPlante" placeholder="Rechercher" aria-label="Rechercher" autocomplete="off" spellcheck="false">
            <a href="accessoire.php">
            </a>
        </div>
            
        <?php //boucle qui affiche les plantes de la BDD
            include('connexion.php');
        $req_boutique = mysqli_query($mysqli, "SELECT * FROM BOUTIQUE");

            echo '<form id="boutiqueBlock">';
        while ($boutique = $req_boutique->fetch_assoc()){
            echo '<div class="box-plant">';
                    echo '<img class="imgPlante" src="img/'.$boutique['img_plante'].'.png"></img>';  
                    echo '<div class="plant-details">';
                        echo '<option id="nom_plante" class="plant-name">'.$boutique['nom_plante'].'</option><br>';
                        echo '<option name="prix_plante" class="plant-price">'.$boutique['prix_plante'].'€</option><br>';     
                        echo '<button type="button" class="btnAddpanier" data-id='.$boutique['id_plante'].'>Ajouter au panier</button>';
                    echo '</div>';
            echo '</div>';
        }
            echo '</form>';

        mysqli_close($mysqli); // Close connection
        ?>


<script type='text/javascript'>
    //post la variable id_plante dans le fichier ajaxfile lorsque que l'ont clique sur le bouton btnAddpanier 
                $(document).on('click', '.btnAddpanier', function(){
                    var id_plante = $(this).data('id');
                    $.ajax({
                        url: 'ajaxfile.php',
                        type: 'post',
                        data: {id_plante: id_plante},
                        success: function(response){ //si tout fonctionne ajoute le modal body avec les infos dans la div modal(empModal) et ouvre le modal
                            $('#empModal').html(response); 
                            $('#empModal').modal('show'); 
                        }
                    });
                });
</script>

<!-- Modal -->
<div class="modal fade" id="empModal" role="dialog">
</div>

</body>

</html>
<script>
$(document).ready(function(){
  $("#searchPlante").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#boutiqueBlock .box-plant").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>