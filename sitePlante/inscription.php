.
<?php
    session_start();
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css">
    <title id="titre">Inscription - FlowerShop</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
</head>
      <?php
      include('navbar.php');
      include('connexion.php');
      ?>
<br>
<h1>Nouveau client ? Créez votre compte</h1>
    <br>
    <div id="container" style="width: 25%; margin-left: auto; margin-right: auto;">
    <!-- zone d'inscription -->
        <form class="formulaire_conn" method="post"> 
        <p>Formulaire d'inscription</p>
                <label><b>Nom d'utilisateur</b></label>
                <input type="text" placeholder="Entrez le nom d'utilisateur" name="Nom" required>
                <label><b>Prenom</b></label>
                <input type="text" placeholder="Entrez le prenom d'utilisateur" name="Prenom" required>
                <label><b>Email</b></label>
                <input type="text" pattern="[^@\s]+@[^@\s]+" placeholder="Exemple.nom@mail.com" name="email" required>
                <label><b>Mot de passe</b></label>
                <input type="password" placeholder="Entrez le mot de passe" minlength="6" name="password" required>
                <label><b>Confirmez votre mot de passe</b></label>
                <input type="password" placeholder="Confirmez votre mot de passe" minlength="6" name="repeatpassword" required><br><br>
                <input type="submit" name="submit" value="Valider">
            
        </form>
        <br>
    </div>

<?php
      
if (isset($_POST['submit']))
{
   /* on test si les champ sont bien remplis */
    if(!empty($_POST['Nom']) and !empty($_POST['Prenom']) and !empty($_POST['email']) and !empty($_POST['password']) and !empty($_POST['repeatpassword']))
    {   
        /* on test si le mdp contient bien au moins 6 caractère */
        if (strlen($_POST['password'])>=6)
        {
            /* on test si les deux mdp sont bien identique */
            if ($_POST['password']==$_POST['repeatpassword'])
            {
                // On crypte le mot de passe
                $_POST['password']= md5($_POST['password']);
                //On créé la requête
                $sqlInscription = "INSERT INTO UTILISATEUR VALUES (0,'".$_POST['Nom']."','".$_POST['Prenom']."','".$_POST['email']."','".$_POST['password']."')";
                $insert = $mysqli -> query($sqlInscription);
                $last_id = mysqli_insert_id($mysqli);//récupere le dernier id utilisateur
                $sqlInscription2 = "INSERT INTO STATUT VALUES (0,'$last_id', 0)";
                $insert2 = $mysqli -> query($sqlInscription2);
                header('Location: valider.php');
                /* execute et affiche l'erreur mysql si elle se produit */
                if(!$c->query($sql))
                {
                    printf("Message d'erreur : %s\n", $c->error);
                }
            // on ferme la connexion
            mysqli_close($c);
            }
            else {
                echo "<h3 class='ErreurInscription'>Les mots de passe ne sont pas identiques</h3>";

            }
        }
        else {
            echo "<h3 class='ErreurInscription'>Le mot de passe est trop court !</h3>";
        }
    }
    else echo "<h3 class='ErreurInscription'>Veuillez saisir tous les champs !</h3>";
}
?>
</body>
</html>
