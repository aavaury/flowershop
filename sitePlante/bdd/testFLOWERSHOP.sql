-- --------------------------------------------------------
-- Hôte:                         192.168.1.150
-- Version du serveur:           10.3.27-MariaDB-0+deb10u1 - Debian 10
-- SE du serveur:                debian-linux-gnu
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------


-- Listage de la structure de la base pour flowershop
CREATE DATABASE IF NOT EXISTS `flowershop` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `flowershop`;

-- Listage de la structure de la table flowershop. BOUTIQUE
CREATE TABLE IF NOT EXISTS `BOUTIQUE` (
  `id_plante` int(11) NOT NULL AUTO_INCREMENT,
  `nom_plante` text NOT NULL,
  `prix_plante` decimal(10,0) NOT NULL,
  `img_plante` varchar(60) NOT NULL,
  PRIMARY KEY (`id_plante`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;

-- Listage de la structure de la table flowershop. COMMANDE
CREATE TABLE IF NOT EXISTS `COMMANDE` (
  `id_commande` int(11) NOT NULL AUTO_INCREMENT,
  `id_utilisateur` int(11) NOT NULL,
  `id_plante` int(11) NOT NULL,
  PRIMARY KEY (`id_commande`),
  KEY `FK_COMMANDE_BOUTIQUE` (`id_plante`),
  KEY `FK_COMMANDE_UTILISATEUR` (`id_utilisateur`),
  CONSTRAINT `FK_COMMANDE_BOUTIQUE` FOREIGN KEY (`id_plante`) REFERENCES `BOUTIQUE` (`id_plante`),
  CONSTRAINT `FK_COMMANDE_UTILISATEUR` FOREIGN KEY (`id_utilisateur`) REFERENCES `UTILISATEUR` (`id_utilisateur`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Listage des données de la table flowershop.COMMANDE : ~0 rows (environ)
/*!40000 ALTER TABLE `COMMANDE` DISABLE KEYS */;
/*!40000 ALTER TABLE `COMMANDE` ENABLE KEYS */;

-- Listage de la structure de la table flowershop. PANIER
CREATE TABLE IF NOT EXISTS `PANIER` (
  `id_panier` int(2) NOT NULL AUTO_INCREMENT,
  `id_utilisateur` int(11) DEFAULT NULL,
  `id_plante` int(11) NOT NULL,
  `nom_plante` text DEFAULT NULL,
  `prix_plante` int(11) NOT NULL,
  PRIMARY KEY (`id_panier`),
  KEY `FK_PANIER_UTILISATEUR` (`id_utilisateur`),
  KEY `FK_PANIER_BOUTIQUE` (`id_plante`),
  CONSTRAINT `FK_PANIER_BOUTIQUE` FOREIGN KEY (`id_plante`) REFERENCES `BOUTIQUE` (`id_plante`),
  CONSTRAINT `FK_PANIER_UTILISATEUR` FOREIGN KEY (`id_utilisateur`) REFERENCES `UTILISATEUR` (`id_utilisateur`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- Listage des données de la table flowershop.PANIER : ~0 rows (environ)
/*!40000 ALTER TABLE `PANIER` DISABLE KEYS */;
/*!40000 ALTER TABLE `PANIER` ENABLE KEYS */;

-- Listage de la structure de la table flowershop. UTILISATEUR
CREATE TABLE IF NOT EXISTS `UTILISATEUR` (
  `id_utilisateur` int(11) NOT NULL AUTO_INCREMENT,
  `nom_utilisateur` text DEFAULT NULL,
  `prenom_utilisateur` text DEFAULT NULL,
  `email_utilisateur` text DEFAULT NULL,
  `mdp_utilisateur` text DEFAULT NULL,
  PRIMARY KEY (`id_utilisateur`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4;

-- Listage des données de la table flowershop.UTILISATEUR : ~1 rows (environ)
/*!40000 ALTER TABLE `UTILISATEUR` DISABLE KEYS */;
INSERT INTO `UTILISATEUR` (`id_utilisateur`, `nom_utilisateur`, `prenom_utilisateur`, `email_utilisateur`, `mdp_utilisateur`) VALUES
	(11, 'Boubou', 'Titouan', 'titouan.boubou@gmail.com', '2c3e711d5c7513d973688bb6320b4cd4');
/*!40000 ALTER TABLE `UTILISATEUR` ENABLE KEYS */;

-- Listage des données de la table flowershop.BOUTIQUE : ~8 rows (environ)
/*!40000 ALTER TABLE `BOUTIQUE` DISABLE KEYS */;
INSERT INTO `BOUTIQUE` (`id_plante`, `nom_plante`, `prix_plante`, `img_plante`) VALUES
	(1, 'Tulipe', 8, 'plante1'),
	(2, 'Hortensia', 13, 'plante2'),
	(3, 'Amarante', 11, 'plante3'),
	(4, 'Coeur de marie', 10, 'plante9'),
	(5, 'Cyclamen', 14, 'plante5'),
	(6, 'Euphorbe', 16, 'plante6'),
	(7, 'Lupin', 5, 'plante7'),
	(8, 'Lotus', 18, 'plante8');

