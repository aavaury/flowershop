<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css">
    <title id="titre">Qui sommes nous ? - FlowerShop</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="https://site-assets.fontawesome.com/releases/v6.0.0/css/all.css">

</head>

    <body>
        

        <?php
        include('navbar.php');
        ?>

        <br><br>
        <div style="text-align: center;font-family: cursive; font-size: 20px;">
            <p> Nous sommes 3 élèves en deuxième année de BTS SIO option SLAM.</p>
            <br>
            <p> Nous avons crée ce site au cours de la deuxieme année comme support pour le projet de fin d'année.</p>
            <br>
        </div>
        <div style="text-align: center;" class="row">
            <p class="col-12 mb-2" style="font-size: 25px;">FROMENTIN Lucas-Enzo</p>
            <a  href="https://lucasxftn.000webhostapp.com/" class="col-12 m-auto" ><i class="fa-solid fa-user col-12 mb-2" style="font-size: 50px; color:black;"></i></a>
            <br>
            <p class="col-12 mb-2" style="font-size: 25px;">VAURY Adrien</p>
            <a  href="http://adrien.vaury.free.fr/" class="col-12 m-auto" ><i class="fa-solid fa-user col-12 mb-2" style="font-size: 50px; color:black;"></i></a>
            <br>
            <p class="col-12 mb-2" style="font-size: 25px;">DENISE Constant</p>
            <a  href="#" class="col-12 m-auto" ><i class="fa-solid fa-user col-12 mb-2" style="font-size: 50px; color:black;"></i></a>
        </div>

        <?php
            if(isset($_SESSION['username'])){
                header("index.php");
            }
            else{
                header('principale.php');
            }
            include('footer.php');

        ?> 
        </body>

</div>
    </html>
    