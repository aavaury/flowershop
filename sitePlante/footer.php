<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css">
    <title id="titre">Qui sommes nous ? - FlowerShop</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="https://site-assets.fontawesome.com/releases/v6.0.0/css/all.css">

</head>

<div style="display:flex;justify-content: space-between;color:#fff;background: #333;position:fixed;bottom:0;width:100vw;padding:10px; ">
            <div>
                <ul style="padding: 10px;display:flex;gap:30px">
                    <li><a href="accueil.php" style=" color:white;">Accueil</a></li>
                    <li><a href="boutique.php" style=" color:white;" >Boutique</a></li>
                    <li><a href="panier.php" style=" color:white;" >Panier</a></li>
                    <li><a href="qui_sommes_nous.php" style=" color:white;" >Qui sommes nous ?</a></li>
                    <li><a href="index.php" style=" color:white;" >Se connecter</a></li>
                </ul>
                <p style="margin:0; color:#A9A9A9; font-size: x-small;" >FlowerShop &copy; 2022</p>
            </div>
            <div style="display:flex; gap: 10px;align-self:center">
                <a href="#" style="padding: 5px 10px; background: rgba(255,255,255,.15);border-radius: 4px;color: #fff"><i class="fa-brands fa-facebook"></i></a>
                <a href="#" style="padding: 5px 10px; background: rgba(255,255,255,.15);border-radius: 4px;color: #fff"><i class="fa-brands fa-twitter"></i></a>
                <a href="#" style="padding: 5px 10px; background: rgba(255,255,255,.15);border-radius: 4px;color: #fff"><i class="fa-brands fa-linkedin"></i></a>
                <a href="https://gitlab.com/aavaury/flowershop" style="padding: 5px 10px; background: rgba(255,255,255,.15);border-radius: 4px;color: #fff"><i class="fa-brands fa-git"></i></a>
            </div>
        </div>