<?php
  if(isset($_SESSION['username'])){
    header('Location: index.php');
}
else{
    session_start();
}
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css">
    <title id="titre">FlowerShop - Mon Compte</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
</head>

    <body>
    <?php
        include('navbar.php');
        ?>
        <h1>FlowerShop - Mon Compte</h1>
        <br><br>
        <div id="content" class="content1">
            <!-- tester si l'utilisateur est connecté -->
            <?php
            include('connexion.php');
                if($_SESSION['username'] !== ""){
                    $user = $_SESSION['username'];
                    // afficher un message
                    $res = mysqli_query($mysqli, "SELECT * FROM UTILISATEUR where nom_utilisateur = '$user' ");

                    while ($row = mysqli_fetch_assoc($res)) {
                    
                        $nom = $row['nom_utilisateur']; // le texte stocké dans la variable $nom
                        $prenom = $row['prenom_utilisateur']; // le texte stocké dans la variable $prenom
                        $email = $row['email_utilisateur']; // le texte stocké dans la variable $email

                    }
                }
            ?>
            <form class ="formulaire formulaire_conn">
            <p> Nom </p>
            <input type="text" value="<?php echo $nom; ?>" name="texte" readonly="readonly"/> <!-- champs de texte contenant le contenu de la variable $nom -->
            <p> Prenom </p>
            <input type="text" value="<?php echo $prenom; ?>" name="texte" readonly="readonly"/> <!-- champs de texte contenant le contenu de la variable $prenom -->
            <p> Email </p>
            <input type="text" value="<?php echo $email; ?>" name="texte" readonly="readonly"/> <!-- champs de texte contenant le contenu de la variable $email -->
            <a class="buttonDisconect" href="logout.php">Se deconnecter</a>
            </form>
        </div>
    </body>
</html>
