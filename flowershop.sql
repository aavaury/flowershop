-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le : ven. 13 mai 2022 à 12:25
-- Version du serveur :  10.3.27-MariaDB-0+deb10u1
-- Version de PHP : 7.3.19-1~deb10u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `flowershop`
--

-- --------------------------------------------------------

--
-- Structure de la table `BOUTIQUE`
--

CREATE TABLE `BOUTIQUE` (
  `id_plante` int(11) NOT NULL,
  `nom_plante` text NOT NULL,
  `prix_plante` decimal(10,0) NOT NULL,
  `img_plante` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `BOUTIQUE`
--

INSERT INTO `BOUTIQUE` (`id_plante`, `nom_plante`, `prix_plante`, `img_plante`) VALUES
(1, 'Tulipe', '8', 'tulipe'),
(2, 'Hortensia', '13', 'hortensia'),
(3, 'Amarante', '11', 'AmaranthusPlainsman'),
(4, 'Coeur de marie', '10', 'coeur-de-marie'),
(5, 'Cyclamen', '14', 'cyclamen'),
(6, 'Euphorbe', '16', 'Euphorbe'),
(7, 'Banzai', '13', 'banzai'),
(8, 'Lotus', '18', 'lotus'),
(9, 'Rose', '14', 'rose'),
(10, 'Orchidee', '15', 'orchidee'),
(11, 'bleuet', '12', 'bleuet'),
(12, 'asters', '15', 'asters'),
(13, 'Jonquille', '12', 'jonquille'),
(14, 'Arums', '13', 'arums'),
(15, 'Anémone', '11', 'anémone'),
(16, 'Ancolie', '12', 'ancolie');

-- --------------------------------------------------------

--
-- Structure de la table `PANIER`
--

CREATE TABLE `PANIER` (
  `id_panier` int(2) NOT NULL,
  `id_utilisateur` int(11) DEFAULT NULL,
  `id_plante` int(11) NOT NULL,
  `qty_panier` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `PANIER`
--

INSERT INTO `PANIER` (`id_panier`, `id_utilisateur`, `id_plante`, `qty_panier`) VALUES
(34, 26, 2, 1),
(35, 26, 3, 1);

-- --------------------------------------------------------

--
-- Structure de la table `STATUT`
--

CREATE TABLE `STATUT` (
  `idStatut` int(11) NOT NULL,
  `idUtilisateur` int(11) NOT NULL,
  `statut` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `STATUT`
--

INSERT INTO `STATUT` (`idStatut`, `idUtilisateur`, `statut`) VALUES
(8, 26, 1),
(9, 27, 0),
(10, 28, 0);

-- --------------------------------------------------------

--
-- Structure de la table `UTILISATEUR`
--

CREATE TABLE `UTILISATEUR` (
  `id_utilisateur` int(11) NOT NULL,
  `nom_utilisateur` text DEFAULT NULL,
  `prenom_utilisateur` text DEFAULT NULL,
  `email_utilisateur` text DEFAULT NULL,
  `mdp_utilisateur` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `UTILISATEUR`
--

INSERT INTO `UTILISATEUR` (`id_utilisateur`, `nom_utilisateur`, `prenom_utilisateur`, `email_utilisateur`, `mdp_utilisateur`) VALUES
(26, 'root', 'admin', 'admin.root@gmail.com', 'e10adc3949ba59abbe56e057f20f883e'),
(27, 'constant', 'constant', 'constant@gmail.com', '617ac08757d38a5a7ed91c224f0e90a0'),
(28, 'Fromentin', 'Lucas-enzo', 'lucasenzo.fromentin@gmail.com', 'e10adc3949ba59abbe56e057f20f883e');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `BOUTIQUE`
--
ALTER TABLE `BOUTIQUE`
  ADD PRIMARY KEY (`id_plante`);

--
-- Index pour la table `PANIER`
--
ALTER TABLE `PANIER`
  ADD PRIMARY KEY (`id_panier`),
  ADD KEY `FK_PANIER_BOUTIQUE` (`id_plante`),
  ADD KEY `FK_Utilisateur_Panier` (`id_utilisateur`);

--
-- Index pour la table `STATUT`
--
ALTER TABLE `STATUT`
  ADD PRIMARY KEY (`idStatut`),
  ADD KEY `FK_statut_utilisateur` (`idUtilisateur`);

--
-- Index pour la table `UTILISATEUR`
--
ALTER TABLE `UTILISATEUR`
  ADD PRIMARY KEY (`id_utilisateur`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `BOUTIQUE`
--
ALTER TABLE `BOUTIQUE`
  MODIFY `id_plante` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT pour la table `PANIER`
--
ALTER TABLE `PANIER`
  MODIFY `id_panier` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT pour la table `STATUT`
--
ALTER TABLE `STATUT`
  MODIFY `idStatut` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT pour la table `UTILISATEUR`
--
ALTER TABLE `UTILISATEUR`
  MODIFY `id_utilisateur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `PANIER`
--
ALTER TABLE `PANIER`
  ADD CONSTRAINT `FK_Utilisateur_Panier` FOREIGN KEY (`id_utilisateur`) REFERENCES `UTILISATEUR` (`id_utilisateur`),
  ADD CONSTRAINT `PANIER_ibfk_1` FOREIGN KEY (`id_plante`) REFERENCES `BOUTIQUE` (`id_plante`);

--
-- Contraintes pour la table `STATUT`
--
ALTER TABLE `STATUT`
  ADD CONSTRAINT `FK_statut_utilisateur` FOREIGN KEY (`idUtilisateur`) REFERENCES `UTILISATEUR` (`id_utilisateur`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
